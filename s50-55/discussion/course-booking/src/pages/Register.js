import {Container, Row, Col} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function Register(){
	const [firstName, setFirst] = useState('');
	const [lastName, setLast] = useState('');
	const [mobNum, setMob] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);

	// we contained the useNavigate to navigae variable;
	const navigate = useNavigate();

	const {user, setUser} = useContext(UserContext);

	/*const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: "GET",
			headers : {'Content-type':'application/json', Authorization : `Bearer ${token}`}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}*/

	useEffect(() => {
		if ((firstName!== '' && lastName!== '' && mobNum != '' && email!== '' && password1 !=='' && password2 !== '') && password1 === password2) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	})

	function register(event){
		event.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobNum,
				email: email,
				password: password1
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title:'Duplicate email found',
					icon: 'error',
					text: 'Please provide a different email or login using current email.'
				})
			}else{
				Swal2.fire({
					title: 'Success',
					icon: 'success',
					text: 'You are now registered!'
				})
				/*localStorage.setItem('token', data.auth);
				retrieveUserDetails(data.auth);*/
				navigate('/login');
			}
		});

		// navigate('/');

		// // clear
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
	}
	return(
		(user.id === null) ?
		<Container className = 'mt-5'> 
			<Row>
				<Col className = 'col-6 mx-auto'>
					<h1 className = 'text-center'>Register</h1>
					<Form onSubmit={register}>
						<Form.Group className="mb-3" controlId="formFirstName">
						  <Form.Label>First Name</Form.Label>
						  <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange = {event => setFirst(event.target.value)}/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formLastName">
						  <Form.Label>Last Name</Form.Label>
						  <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange = {event =>	setLast(event.target.value)}/>
						</Form.Group>
						<Form.Group className="mb-3" controlId="formMobNumb">
						  <Form.Label>Mobile No.</Form.Label>
						  <Form.Control type="text" placeholder="Enter Mobile Number" value={mobNum} onChange = {event => setMob(event.target.value)}/>
						</Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control type="email" placeholder="Enter email" value={email} onChange = {event =>	setEmail(event.target.value)}/>
					      </Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control type="password" placeholder="Password" value = {password1} onChange = {event => setPassword1(event.target.value)}/>
					      </Form.Group>
					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Confirm Password</Form.Label>
					        <Form.Control type="password" placeholder="Retype your password" value = {password2} onChange = {event => setPassword2(event.target.value)} />
					      </Form.Group>
					      <p>Have an account already? <Link to = '/login'>Log in here</Link>.</p>
					      <Button variant="primary" type="submit" disabled = {isDisabled}>
					        Submit
					      </Button>
					    </Form>
				</Col>
			</Row>
		</Container>
		:
		<Navigate to = "*"/>
	)
}