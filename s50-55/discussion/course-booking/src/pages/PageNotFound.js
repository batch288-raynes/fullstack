import {Link} from 'react-router-dom';
import {Container, Row, Col, Card} from 'react-bootstrap';

export default function PageNotFound(){
	return (
		<>
			<Container>
				<Row>
					<Col>
						<h1>Page Not Found</h1>
						<p>Go back to the <a href='/'>homepage</a></p>
					</Col>
				</Row>
			</Container>
		</>
		)
}