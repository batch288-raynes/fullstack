// import coursesData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js';
import {useState, useEffect} from 'react';

export default function Courses(){
	/*const courses = coursesData.map(course => {
		return (
			<CourseCard key = {course.id} courseProp = {course} />
			)
	})*/
	// .map method loops through individual elements
	const [courses, setCourses] = useState([])
	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourses`)
		.then(result => result.json())
		.then(data => {
			setCourses(data.map(course => {
				return (
					<CourseCard key = {course.id} courseProp = {course} />
					)
			}))
		})
	}, [])
	return (
		<>
		{courses}
		</>
		)
}