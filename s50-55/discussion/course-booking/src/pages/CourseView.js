import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';



export default function CourseView(){
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [seat, setSeat] = useState(0);
	const {courseId} = useParams();

	const navigate = useNavigate();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [])

	function add(){
		if (seat < 30){
			setSeat(seat+1);
		// } else if(seat ==29){
		// 	setSeat(seat+1);
		// 	setIsDisabled(true);
		} else {
			alert('No more seats...')
		}
	}
	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: 
				{'Authorization' : `Bearer ${localStorage.getItem('token')}`,
				'Content-type' : 'application/json'
				},
			body : JSON.stringify({
				id: courseId

			})

		})
		.then(response => response.json())
		.then(data => {
			if(data){
				Swal2.fire({
					icon: 'success',
					title: 'Success!',
					text: 'You have successfully enrolled'
				})
				navigate('/courses');
			} else {
				Swal2.fire({
					icon: 'error',
					title: 'Something went wrong',
					text: 'Please try again.'
				})
			}
		})
	}

	return (
		<Container className = 'mt-5'>
			<Row>
				<Col>
					<Card>
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>{price}</Card.Text>
					        <Card.Subtitle>Class Schedule</Card.Subtitle>
					        <Card.Text>8AM - 5PM</Card.Text>
					        <Card.Subtitle>
					        	Enrollees
					        </Card.Subtitle>
					        <Card.Text>
					        	no. of enrollees: {seat}
					        </Card.Text>
					        <Button onClick = {() => {
					        	enroll(courseId)
					        	add()}} variant="primary">Enroll</Button>
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>
		)
}