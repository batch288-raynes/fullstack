import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import CourseView from './pages/CourseView.js';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
// import UserProvider
import {UserProvider} from './UserContext.js';


// BrowserRouter simulates page navigation by synchronizing the shown content and the shown URL in the web browser.
// The Routes component holds all our route components. It selects which "Route" component to show based on the url endpoint.



function App() {
  useEffect(()=> {
    if (localStorage.getItem('token')) {
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        method: "GET",
        headers : {'Content-type':'application/json', Authorization : `Bearer ${localStorage.getItem('token')}`}
      })
      .then(result => result.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      })
    }
  }, []);
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  });
  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <UserProvider value = {{user, setUser,unsetUser}}>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route path ='/' element = {<Home/>} />
          <Route path ='/courses' element = {<Courses/>} />
          <Route path ='/register' element = {<Register/>} />
          <Route path ='/login' element = {<Login/>} />
          <Route path = '/logout' element = {<Logout/>} />
          <Route path ='*' element = {<PageNotFound />} />
          <Route path ='/courseView/:courseId' element = {<CourseView />} />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
