import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState,useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom';


export default function CourseCard(props){
	const {_id, name, description, price} = props.courseProp;
	// Use the state hook for this component to be able to store its state
	// States are use to keep track of information related to individual components
		// Syntax : const [getter, setter] = useState(initialGetterValue);

	const [seat, setSeat] = useState(0);
	const [isDisabled, setIsDisabled] = useState(false)

	const {user} = useContext(UserContext);
	console.log(props.courseProp);

	function enroll(){
		if (seat < 30){
			setSeat(seat+1);
		// } else if(seat ==29){
		// 	setSeat(seat+1);
		// 	setIsDisabled(true);
		} else {
			alert('No more seats...')
		}
	}

	// the function or the side effect in our useEffect hook will be invoked or run on the initial loading of the application and when there is/are change/changes on our dependencies

	useEffect(() => {
			if (seat == 30){
				setIsDisabled(true);
			}
		}, [seat]);
	return (
		<Container className="mt-3">
			<Row>
				<Col className = "col-12">
					<Card className='cardHighlight'>
					      <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Text>
					        	Description:
					        	<br/>
					        	{description}
					        </Card.Text>
					        <Card.Text>
					        	Price:
					        	<br/>
					        	{price}
					        </Card.Text>
					        <Card.Subtitle>
					        	Enrollees
					        </Card.Subtitle>
					        <Card.Text>
					        	no. of enrollees: {seat}
					        </Card.Text>
					        {
					        	user.id === null ?
					        	<Button as = {Link} to = '/login'>Login to Enroll</Button>
					        	:
					        	<Button as = {Link} to = {`/courseView/${_id}`} disabled={isDisabled}>Details</Button>

					        }
					        
					      </Card.Body>
					    </Card>
				</Col>
			</Row>
		</Container>
		)
}