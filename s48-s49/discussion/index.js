fetch('https://jsonplaceholder.typicode.com/posts')
.then(result => result.json())
.then(response => {
	console.log(response);
	showPosts(response);
})

const showPosts = (posts) =>{
	let entries = '';
	posts.forEach(i => {
		entries += `
		<div>
			<h3 class= "post-${i.id}" id = "post-title-${i.id}">${i.title}</h3>
			<p class= "post-${i.id}" id = "post-body-${i.id}">${i.body}</p>
			<button class= "post-${i.id}" onclick = "editPost(${i.id})">Edit</button>
			<button class= "post-${i.id}" onclick = "deletePost(${i.id})">Delete</button>
		</div>
		`
		document.querySelector('#div-post-entries').innerHTML = entries;
	})
}

document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {method:'POST', headers: {'Content-type': 'application/json' },
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		})
	})
	.then(response => response.json())
	.then(result => {
		console.log(result);
		alert('Post is successfully posted!')
	})
})

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;

	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	console.log(title);
	console.log(body);

	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
	document.querySelector('#txt-edit-id').value = id;
}

// updating the post
document.querySelector('#form-edit-post').addEventListener('submit', (event)=>{
	// to prevent autoreload
	event.preventDefault();

	let id = document.querySelector('#txt-edit-id').value
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method:'PUT', 
		headers:{'Content-Type': 'application/json'},
		body: JSON.stringify({
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		})
		})
	.then(response => response.json())
	.then(result => {
		console.log(result);
		alert('The post is succesfully updated!');

		document.querySelector(`#post-title-${id}`).innerHTML = document.querySelector('#txt-edit-title').value
		document.querySelector(`#post-body-${id}`).innerHTML = document.querySelector('#txt-edit-body').value

		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		// setAttribute method adds attribute to the selected element
		document.querSelector('#btn-submit-update').setAttribute('disabled');
	})
})

// activity

const deletePost = (id) => {
	document.querySelectorAll(`.post-${id}`).forEach(item => item.remove());
}