// console.log('Hello!@');


// Section: Document OBject Model (DOM)
	// allows us to access ror modify the properties of an html element in a webpage
	// it is standard on how to get, change, add or delete objects

	// using the querySelector it can access/get the HTML element/s
		// CSS selectors to target specific element
			// id selector(#);
			// class selector(.)
			// tag/type selector(html tags)
			// universal selector(*)
			// attribute selector([attribute])

	// Query selectors has two types: querySelector and querySelectorAll
let firstElement = document.querySelector('#txt-first-name');
console.log(firstElement);

let secondElement = document.querySelector('.full-name');
let thirdElement = document.querySelectorAll('.full-name')

// getElements
let element = document.getElementById('fullName');
console.log(element);

element = document.getElementsByClassName('full-name');
console.log(element);

// Event Listeners
	// whenever a user interacts with a webpage, this action is considered as event
	// working with events is a large part of creating  interactivity in a webpage
	// specific function will be invoked if the even happens.
	// function 'addEventListener', it takes two arguments
		// first argument a string identifiying the event
		// second, argument function that the listener will invoke once the specified event occurs.

firstElement.addEventListener('keyup', ()=>{
	let valInput = firstElement.value
	console.log(firstElement.value);
});

let txtLastName = document.querySelector('#txt-last-name');

txtLastName.addEventListener('keyup', ()=>{
	console.log(txtLastName.value);
});

let fullName = document.querySelector('#fullName');

txtLastName.addEventListener('keyup', ()=> {
	fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
})
console.log(fullName)
let changeColor = document.querySelector('#text-color');
changeColor.addEventListener('change',(event)=> {
	if (changeColor.value == 'red') {
		fullName.style.color = 'red'
	} else if (changeColor.value == 'green'){
		fullName.style.color = 'green'
	} else if (changeColor.value == 'blue'){
		fullName.style.color = 'blue'
	} else {
		fullName.style.color = 'black'
	}
})