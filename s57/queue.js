let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    
    collection[collection.length] = element;
    
    return collection
}

function dequeue() {
    // In here you are going to remove the first element in the array
    temp = []
    temp1 = []
    for (i = collection.length-1; i>= 0; i--) {
        temp[temp.length] = collection[i]
    }
    temp.length = temp.length - 1;
    for (i = temp.length-1; i>= 0; i--) {
        temp1[temp1.length] = temp[i]
    }
    collection = temp1
    return collection
    
}

function front() {
    // In here, you are going to return first element
    
    return collection[0]
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements
    size = 0;
    i = 0;
    while (collection[i] != undefined){
        size++;
        i++;
    }
    return size   
}

function isEmpty() {
    return (collection[0]==undefined)
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};